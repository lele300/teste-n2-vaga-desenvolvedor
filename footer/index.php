<footer class="blog-footer">
        <a href="#topo">Back to top</a>
      </p>
</footer>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script>

/* Este bloco impedia que o link funcionasse por evitar o comportamento padrão de href
$(document).on('click','a.text-dark',function(e){

	if($(this).attr("href") == "http://www.estadao.com.br"){
 		e.preventDefault(); //Evita o comportamento default do atributo href
	}

}); */

// Para cada elemento que tenha as classes 'd-inline-block', '.mb-2' e '.text-sucess, executa a função
$(".d-inline-block.mb-2.text-success").each(function(){

	//Se o texto populado pelo JSON for Politica, adiciona a classe (text-warning) para cor amarela;
	if($(this).text() == "Política")
	{ 
		$(this).removeClass("text-success");
		$(this).addClass("text-warning");
		
	// Se o texto populado pelo JSON for Home, adiciona a classe (text-danger) para cor vermelha;	
	} else if ($(this).text() == "Home"){
		$(this).removeClass("text-success");
		$(this).addClass("text-danger");
	}
});


</script>